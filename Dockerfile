ARG FROM_IMAGE_NAME=gcr.io/deeplearning-platform-release/pytorch-gpu.1-4
FROM ${FROM_IMAGE_NAME}


ENV DEBIAN_FRONTEND noninteractive
# WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

#--------- Install productivity tools --------------
RUN apt-get update && apt-get install -qy htop dstat sysstat && \
  rm -rf /var/lib/apt/lists/*

#--------- Install CUDA dev/build tools to compile detectron2 -------------
RUN apt-get update && apt-get install -qy cuda-libraries-dev-10-1 cuda-command-line-tools-10-1 cuda-minimal-build-10-1 && \
  rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y \
	libpng-dev libjpeg-dev python3-opencv ca-certificates \
	python3-dev build-essential pkg-config git curl wget automake libtool && \
  rm -rf /var/lib/apt/lists/*

#--------- Install latest Pip ---------------
RUN curl -fSsL -O https://bootstrap.pypa.io/get-pip.py && \
	python3 get-pip.py && \
	rm get-pip.py

#--------- Install dependencies -------------
RUN pip install cython \
	'git+https://github.com/facebookresearch/fvcore'


#--------- Install detectron2 ---------------
RUN git clone https://github.com/facebookresearch/detectron2 /detectron2_repo
ENV FORCE_CUDA="1"
ENV TORCH_CUDA_ARCH_LIST="Maxwell;Maxwell+Tegra;Pascal;Volta;Turing"
ENV LD_LIBRARY_PATH="/usr/local/cuda/lib64:$LD_LIBRARY_PATH"
RUN cd /detectron2_repo && python setup.py build develop
RUN pip install -e /detectron2_repo


#--------- Install optional libraries for data access --------------
RUN pip install 'git+https://github.com/cocodataset/cocoapi.git#subdirectory=PythonAPI'
RUN pip install imantics scipy cityscapesscripts shapely


