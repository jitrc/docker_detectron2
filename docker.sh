#!/bin/bash
# Fail on first error.
set -e

REPO=registry.gitlab.com/jitrc/docker_detectron2
IMAGE_TAG="latest"
BASE_IMAGE=gcr.io/deeplearning-platform-release/pytorch-gpu.1-4


IMAGE="${REPO}:${IMAGE_TAG}"
BUILD_OPTS=""
echo "Image name : $IMAGE"

function show_usage()
{
cat <<EOF
Usage: $(basename $0) [options] ...
OPTIONS:
	build          Build Image
	pull           Pull Image
	push           Push Image
	del            Delete/Remove Image (locally)
	run            Run the image
	-n             Build with no cache

EOF
exit 0
}

function pull()
{
	echo "Pulling image ${IMAGE}"
	docker pull ${IMAGE}
}

function push()
{
	echo "Pushing image ${IMAGE}"
	docker push ${IMAGE}
}

function run()
{
   echo "Run ${IMAGE}"
   docker run --gpus all \
	-it --rm --network host \
	-v /media:/media \
	-v /nfs:/nfs \
	-v `pwd`/workspace:/home \
	-e HISTFILE=/home/.bash_hist \
	--shm-size 6G \
	${IMAGE}
}

function build()
{
	echo "Builing image ${IMAGE}"
	docker build  ${BUILD_OPTS} --build-arg FROM_IMAGE_NAME=${BASE_IMAGE} -t ${IMAGE} .
	echo "Built new image ${IMAGE}"
}

function del()
{
	echo "Deleting image ${IMAGE}"
	docker rmi ${IMAGE}
}


while [ $# -gt 0 ]
do
	case "$1" in
		build)
			build
			exit 0
			;;
		pull)
			pull
			exit 0
			;;
		push)
			push
			exit 0
			;;
		run)
			run
			exit 0
			;;
		del)
			del
			exit 0
			;;
		-h|--help)
			show_usage
			;;
		-n)
			BUILD_OPTS="--no-cache"
			;;
		*)
			echo -e "\033[93mWarning\033[0m: Unknown option: $1"
			show_usage
			exit 2
			;;
	esac
	shift
done

show_usage
